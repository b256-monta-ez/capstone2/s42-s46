// Dependencies
const User = require("../models/User.js");
const auth = require("../auth.js")
const bcrypt = require("bcrypt");
const Product = require("../models/Product.js")


// Registering a New User
module.exports.registerUser = (requestBody) => {

	let newUser = new User({

		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		if(err) {

			return false;
		} else {

			return true;
		}
	})
}

// Authenticate a user
module.exports.authenticateUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then(result => {

		if(result == null) {

			return false;
		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			if(isPasswordCorrect) {

				return {access: auth.createAccessToken(result)}
			} else {

				return false;
			}
		}
	})
}

// Creating an order (NON-ADMIN only)
module.exports.createOrder = async (data) => {

let amount = await Product.findById(data.productId).then(product => {
		return product.price
	});


let totalAmount = amount * data.quantity;


	let orderProduct = await User.findById(data.userId).then(user => {
		
		user.orderedProducts.push({
			
			products: {
				productId: data.productId,
				productName: data.productName,
				quantity: data.quantity
			},
			totalAmount: totalAmount

		});
		


		return user.save().then((checkedOut, err) => {

			if(err) {

				return false;

			} else {

				return true

			}
		})
	});

	let productOrder = await Product.findById(data.productId).then(product => {

		product.userOrders.push({
			userId: data.userId,
			orderId: data.productId
		});

		return product.save().then((ordered, err) => {

			if(err) {

				return false;

			} else {

				return true;

			}
		})
	});

	if(orderProduct && productOrder) {

		return true;

	} else {

		return false;

	}
}



// Retrieve user details
module.exports.getUser = (data) => {

	return User.findById(data.userId).then(result => {

		
		result.password = "";

		return result;

	});

};
