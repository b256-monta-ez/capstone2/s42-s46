// Dependencies
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js")
const User = require("../models/User.js");

// Retrieve all products(active and non-active)
module.exports.getProducts = () => {

	return Product.find({}).then(result => {

		return result;
	})
}

// Retrieve all active products
module.exports.activeProducts = () => {

	return Product.find({isActive: true}).then(result => {

		return result;
	})
}

// Creating a product
module.exports.addProduct = (product) => {

	let newProduct = new Product({

		name: product.name,
		description: product.description,
		price: product.price
	})

	return newProduct.save().then((result, err) => {

		if(err) {

			return false;
		} else {

			return true;
		}
	})
}

// Retrieving a single product
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		return result;
	})
}

// Updating product information
module.exports.updateProduct = (product) => {

	let updatedProduct = {

		name: product.name,
		description: product.description,
		price: product.price
	}

	return Product.findByIdAndUpdate(product.id, updatedProduct).then((result, err) => {

		if(err) {

			return false;
		} else {

			return true;
		}
	})
}

// Archiving a Product
module.exports.archiveProduct = (product) => {

	let updatedStatus = {
		isActive: false
	}

	return Product.findByIdAndUpdate(product.productId, updatedStatus).then((result, err) => {

		if(err) {

			return false;
		} else {

			return true;
		}
	})
}

