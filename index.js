// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");


// Server
const app = express();

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Database
mongoose.connect("mongodb+srv://justcbthings:admin1234@b256montanez.dokawqq.mongodb.net/B256_EcommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log(`Connected to the cloud database`));


// Parent Routes Middlewares
app.use("/users", userRoutes);
app.use("/products", productRoutes);

// Server Listening
app.listen(4000, () => console.log(`API is now connected on port 4000`))