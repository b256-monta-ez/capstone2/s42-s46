// Dependencies
const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers.js");
const auth = require("../auth.js")

// Retrieve all products
router.get("/all-products", (req, res) => {

	productController.getProducts().then(resultFromController => res.send(resultFromController));
});

// Retrieve all active products
router.get("/active-products", (req, res) => {

	productController.activeProducts().then(resultFromController => res.send(resultFromController));
});

// creating a product
router.post("/add-product", auth.verify,  (req, res) =>{

	const data = {

		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {
		productController.addProduct(data.product).then(resultFromController => res.send(resultFromController));
	} else {

		res.send(false);
	}
});

// Route for retrieving a single product
router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating product information
router.put("/:productId", auth.verify, (req, res) => {

const data = {

	product: req.body,
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}

	if(data.isAdmin) {
		productController.updateProduct(data).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);
	}


})

// Route for archiving a product
router.post("/:productId/archive", auth.verify, (req, res) => {

	const data = {

		params: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){ 
		productController.archiveProduct(data.params).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);
	}
})



module.exports = router;