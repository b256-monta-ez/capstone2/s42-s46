// Dependencies
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js");

// Route for user registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;

// Authenticate a user
router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for creating an order
router.post("/checkout", auth.verify, (req, res) => {

	const data = {
	userId: auth.decode(req.headers.authorization).id,
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
	productId: req.body.productId,
	quantity: req.body.quantity,
	productName: req.body.productName
	
}

	if (data.isAdmin === false) {
	userController.createOrder(data).then(resultFromController => res.send(resultFromController));
	
	} else {

		res.send(false);
	}
})


// Retrieve user details
router.get("/:userId/userDetails", auth.verify,  (req, res) => {

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		params: req.params
	}
	
	userController.getUser(data).then(resultFromController => res.send(resultFromController));
})